com.conformiq.creator.structure.v15
creator.customaction qml_09a24273fdbe450b955290dc1df53ade
"_verfication_Review Request"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Review Request has completed " ]
{
}
creator.customaction qml_784172b2d1c74c33b9c621ee443196ae "Review Request"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Review Request" ]
{
}
creator.customaction qml_71cca72205b045de9b3ae9a20fd145d2
"_verfication_Platform Transportation"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Platform Transportation has completed " ]
{
}
creator.externalinterface qml_53195b0799104c4fa334cb7ec577e763 "User"
	direction = in;
creator.customaction qml_b1c8275b1bcd4340a7390e262d8c4988
"_verfication_Car is allocated"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Car is allocated has completed " ]
{
}
creator.customaction qml_ea7a0666a1df4306888f0d23174c688e
"_verfication_Register request"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Register request has completed " ]
{
}
creator.customaction qml_d84f459a95d846efadf3f1528ff61e1a "Allocate car"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Allocate car" ]
{
}
creator.customaction qml_9198afc275854e5a883e893002a19630
"_verfication_Request Rejected"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Request Rejected has completed " ]
{
}
creator.customaction qml_aa6d122bcaa54500b7c3e472c63db2b2 "Is Option?"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "AI"
	direction = in
	tokens = [ literal "Option is " ]
{
}
creator.customaction qml_c135ca499603405e87f4b062ec554c42 "Car is allocated"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Car is allocated" ]
{
}
creator.externalinterface qml_b555668857f843f5add6078e48e3c294 "System"
	direction = out;
creator.customaction qml_c43a62a473234b6f900cebd02d9247fe "Request Rejected"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Request Rejected" ]
{
}
creator.customaction qml_c65e4aff2d724a23bcc4c86a51cb1dce "Register request"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Register request" ]
{
}
creator.customaction qml_2eab88441c8e4a55ac3a45695087f5ce
"_verfication_Allocate car"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Allocate car has completed " ]
{
}
creator.customaction qml_fab79999a8374f688824f19ced83d955
"Platform Transportation"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Platform Transportation" ]
{
}