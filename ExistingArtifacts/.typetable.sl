com.conformiq.creator.structure.v15
creator.externalinterface
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
"http://petstore.swagger.io/api request"
	direction = in;
creator.externalinterface
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
"http://petstore.swagger.io/api response"
	direction = out;
creator.message
qml_openapi_import__GET_20__2f_pets_3f_tags_3d__7b_tags_7d__26_limit_3d__7b_limit_7d__20_Request__message
"GET /pets?tags={tags}&limit={limit} Request"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_3f_tags_3d__7b_tags_7d__26_limit_3d__7b_limit_7d__20_Request__primitivefield__tags
	"tags"
		type = String
		optional array;
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_3f_tags_3d__7b_tags_7d__26_limit_3d__7b_limit_7d__20_Request__primitivefield__limit
	"limit"
		type = number
		optional;
}
creator.message
qml_openapi_import__GET_20__2f_pets_20_Response_20__5b_200_5d___message
"GET /pets Response [200]"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_20_Response_20__5b_200_5d___primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__GET_20__2f_pets_20_Response_20__5b_200_5d___structuredfield__body
	"body"
		type = qml_openapi_import__Pet__sequencetype
		optional array;
}
creator.sequencetype qml_openapi_import__Pet__sequencetype "Pet"
{
	creator.primitivefield qml_openapi_import__Pet__primitivefield__name "name"
		type = String;
	creator.primitivefield qml_openapi_import__Pet__primitivefield__tag "tag"
		type = String
		optional;
	creator.primitivefield qml_openapi_import__Pet__primitivefield__id "id"
		type = number;
}
creator.message qml_openapi_import__GET_20__2f_pets_20_Response__message
"GET /pets Response"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_20_Response__primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__GET_20__2f_pets_20_Response__structuredfield__body "body"
		type = qml_openapi_import__Error__sequencetype;
}
creator.sequencetype qml_openapi_import__Error__sequencetype "Error"
{
	creator.primitivefield qml_openapi_import__Error__primitivefield__code "code"
		type = number;
	creator.primitivefield qml_openapi_import__Error__primitivefield__message
	"message"
		type = String;
}
creator.message qml_openapi_import__POST_20__2f_pets_20_Request__message
"POST /pets Request"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
]
{
	creator.structuredfield
	qml_openapi_import__POST_20__2f_pets_20_Request__structuredfield__pet "pet"
		type = qml_openapi_import__NewPet__sequencetype;
}
creator.sequencetype qml_openapi_import__NewPet__sequencetype "NewPet"
{
	creator.primitivefield qml_openapi_import__NewPet__primitivefield__name
	"name"
		type = String;
	creator.primitivefield qml_openapi_import__NewPet__primitivefield__tag "tag"
		type = String
		optional;
}
creator.message
qml_openapi_import__POST_20__2f_pets_20_Response_20__5b_200_5d___message
"POST /pets Response [200]"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__POST_20__2f_pets_20_Response_20__5b_200_5d___primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__POST_20__2f_pets_20_Response_20__5b_200_5d___structuredfield__body
	"body"
		type = qml_openapi_import__Pet__sequencetype;
}
creator.message qml_openapi_import__POST_20__2f_pets_20_Response__message
"POST /pets Response"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__POST_20__2f_pets_20_Response__primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__POST_20__2f_pets_20_Response__structuredfield__body
	"body"
		type = qml_openapi_import__Error__sequencetype;
}
creator.message
qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Request__message
"GET /pets/{id} Request"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Request__primitivefield__id
	"id"
		type = number;
}
creator.message
qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_200_5d___message
"GET /pets/{id} Response [200]"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_200_5d___primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_200_5d___structuredfield__body
	"body"
		type = qml_openapi_import__Pet__sequencetype;
}
creator.message
qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response__message
"GET /pets/{id} Response"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response__primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response__structuredfield__body
	"body"
		type = qml_openapi_import__Error__sequencetype;
}
creator.message
qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Request__message
"DELETE /pets/{id} Request"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Request__primitivefield__id
	"id"
		type = number;
}
creator.message
qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_204_5d___message
"DELETE /pets/{id} Response [204]"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_204_5d___primitivefield__status
	"status"
		type = number;
}
creator.message
qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response__message
"DELETE /pets/{id} Response"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response__primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response__structuredfield__body
	"body"
		type = qml_openapi_import__Error__sequencetype;
}
creator.gui.screen qmlc244275e5d6642268bc0a504df82918e "Test"
	deleted
{
	creator.gui.button qml4cd3e2d41ed94a35bb8ef0d655ecc8cd "Btn"
		status = dontcare
		deleted;
}
creator.gui.screen qml9f89dfc2981a447b9a1e793107507205 "unnamed"
	deleted
{
}
creator.gui.screen qmlb27750dde7cd41848dcd3392c5aee6dc "Parabank Login"
{
	creator.gui.form qmlf8a77908353b41bab4f2e8ab9e94d784 "Customer Login"
	{
		creator.gui.textbox qmlb1cbdf14f8ff40b8ba95a115009737b2 "Username"
			type = String
			status = dontcare;
		creator.gui.textbox qml945124b492bb458d983bc5e9abda942b "Password"
			type = String
			status = dontcare;
		creator.gui.button qml7ac2b063ae5a47f484bcc6d8e8c32ed6 "Login"
			status = dontcare
			deleted;
		creator.gui.textbox qml9879a468febc4688be8e6660e619b405 "Email"
			type = String
			status = dontcare
			deleted;
	}
	creator.gui.button qmlda59caa5148047ea932dd4cb85d343c1 "Login"
		status = dontcare;
	creator.gui.hyperlink qmla52a6924f78949cb98cdb49428c70c6e "ATM Services"
		status = dontcare
		deleted;
	creator.gui.hyperlink qml1dc8c5c3de3b45158ef89a50a6372e8a "Register"
		status = dontcare;
	creator.gui.labelwidget qml26327b135d26479c8c19dbd25ed9d516 "ErrorMessage:"
		status = dontcare;
}
creator.gui.screen qml1e809d6c3f864f499051cc0dd3f9cf1a "ATM Services"
	deleted
{
}
creator.gui.screen qmlf7d1b0a0ed814cdb8d424666ceb7dcc1 "Login Page"
	deleted
{
}
creator.gui.screen qmlb9dcd1a7b6a84b5c9f9d0c47d74e1f49 "Login Screen"
	deleted
{
}
creator.gui.screen qmlbd862f146eaa41fcbc79045512641b73 "Home"
{
	creator.gui.hyperlink qmlf3c77c414aa74fdb9dc549ed29e84024 "Open New Account"
		status = dontcare;
	creator.gui.hyperlink qmlaba11f385d3b4964bd17c18b968f6894 "Transfer Funds"
		status = dontcare;
	creator.gui.hyperlink qml3f8db28bbaf141a0ba82381a50c0f0d2 "Logout"
		status = dontcare;
	creator.gui.labelwidget qml291259d744da46329663b0f270862f5e "Status"
		status = dontcare;
}
creator.gui.screen qml6ad7aac78781462d813e49e4d4d15d86 "Open New Account"
{
	creator.gui.form qml38b93052663f47b8ab329722077aaa00 "Open New Account"
	{
		creator.gui.dropdown qmlee3941f124ba439787ca48cc5c763472 "Type of Account"
			type = qml9f9d0cc73a494c4f92823222a8b960f9
			status = dontcare;
		creator.gui.dropdown qml4bb1db8267f9419c8a6ffdc90c6da39d "Existing Account"
			type = qml5e55d9a9cffa40668ded86debeef602b
			status = dontcare;
	}
	creator.gui.button qml054bd884d87947279ae595786760e99b "Open New Account"
		status = dontcare;
}
creator.enum qml9f9d0cc73a494c4f92823222a8b960f9 "Account Type"
{
	creator.enumerationvalue qml9c4bd3f77bdf4921ac7bef5258ba2dde "Checking";
	creator.enumerationvalue qmla8fd0561105240138be2f94c95b3e32f "Savings";
}
creator.enum qml5e55d9a9cffa40668ded86debeef602b "Exicsting Account"
{
	creator.enumerationvalue qmle98f76e0980e4224af507dc1e6c35fa1 "123654";
}
creator.gui.screen qmlaf96b14834194934bced315a8fa8ea3c "Transfer Funds"
{
	creator.gui.form qmld40308ebe6b34be9bbda8e928e0e6aec "Transfer Funds"
	{
		creator.gui.textbox qmlb55d64ddd6614a1ea54d4f9e75414c8c "Amount"
			type = String
			status = dontcare;
		creator.gui.dropdown qmldac9cff68ba245ca9630b52c08e0802f "From Account"
			type = qml5e55d9a9cffa40668ded86debeef602b
			status = dontcare;
		creator.gui.dropdown qml55216090146d455ea15368e4c00d9e9a "To Account"
			type = qml5e55d9a9cffa40668ded86debeef602b
			status = dontcare;
	}
	creator.gui.button qml71f9e14ece184254bc302614e04a89e4 "Transfer"
		status = dontcare;
}
creator.gui.screen qml8ad7a43a584f42faa7d3ab41dee03943 "Modelling"
	deleted
{
}
creator.customaction qml0a126480f9544b1285cefabc5ec869d3 "Language"
	interfaces = [ qml77613dee9eb0420fb550d05e9f344dfd ]
	shortform = "LI"
	direction = in
	tokens = [ literal "Application is in " reference
qml4c60eae96db84d0f8a914d5c07f10c5e ]
{
	creator.primitivefield qml4c60eae96db84d0f8a914d5c07f10c5e "Language"
		type = String;
}
creator.externalinterface qml77613dee9eb0420fb550d05e9f344dfd "User123"
	direction = in;
creator.gui.screen qml32f7f677a8e14acea995abfb362810c3 "Order Details"
{
	creator.gui.form qmledfe2a18b2354357b15eb604c98e7ac8 "Order Info"
	{
		creator.gui.textbox qml9306720214124172a1914b98e4167445 "Ord"
			type = String
			status = dontcare;
		creator.gui.textbox qml58d17d2c07994ab6b428cf82b4464abb "Qty"
			type = String
			status = dontcare;
	}
}
creator.gui.screen qmlb9bcfe5c4c414654944263a51f218bc2 "Payment"
{
	creator.gui.form qmlb023fbb10feb451181df0dce720e0ba1 "Payment Details"
	{
		creator.gui.calendar qml68579fed6fef4bc5ab92368904cd34a7 "Pay"
			status = dontcare
			deleted;
		creator.gui.dropdown qml398a8aea68824db58bac0205ee8277f6 "Day"
			status = dontcare
			deleted;
		creator.gui.dropdown qml58edbaca09d44752ad62061cae3c4f48 "Month"
			status = dontcare
			deleted;
		creator.gui.dropdown qmlb6e4a02a99014555aa7bf0bbf6ffa0ef "Year"
			status = dontcare
			deleted;
		creator.gui.calendar qmldc998eb6894c469e87505c4ba80a7a94 "Cal"
			status = dontcare;
	}
}
creator.enum qml00466e31e3944d5f8d9773c4ffc02995 "Day"
	deleted
{
}
creator.customaction qml_09a24273fdbe450b955290dc1df53ade
"_verfication_Review Request"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Review Request has completed " ]
{
}
creator.customaction qml_784172b2d1c74c33b9c621ee443196ae "Review Request"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Review Request" ]
{
}
creator.customaction qml_71cca72205b045de9b3ae9a20fd145d2
"_verfication_Platform Transportation"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Platform Transportation has completed " ]
{
}
creator.externalinterface qml_53195b0799104c4fa334cb7ec577e763 "User"
	direction = in;
creator.customaction qml_b1c8275b1bcd4340a7390e262d8c4988
"_verfication_Car is allocated"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Car is allocated has completed " ]
{
}
creator.customaction qml_ea7a0666a1df4306888f0d23174c688e
"_verfication_Register request"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Register request has completed " ]
{
}
creator.customaction qml_d84f459a95d846efadf3f1528ff61e1a "Allocate car"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Allocate car" ]
{
}
creator.customaction qml_9198afc275854e5a883e893002a19630
"_verfication_Request Rejected"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Request Rejected has completed " ]
{
}
creator.customaction qml_aa6d122bcaa54500b7c3e472c63db2b2 "Is Option?"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "AI"
	direction = in
	tokens = [ literal "Option is " ]
{
}
creator.customaction qml_c135ca499603405e87f4b062ec554c42 "Car is allocated"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Car is allocated" ]
{
}
creator.externalinterface qml_b555668857f843f5add6078e48e3c294 "System"
	direction = out;
creator.customaction qml_c43a62a473234b6f900cebd02d9247fe "Request Rejected"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Request Rejected" ]
{
}
creator.customaction qml_c65e4aff2d724a23bcc4c86a51cb1dce "Register request"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Register request" ]
{
}
creator.customaction qml_2eab88441c8e4a55ac3a45695087f5ce
"_verfication_Allocate car"
	interfaces = [ qml_b555668857f843f5add6078e48e3c294 ]
	shortform = "VA"
	direction = out
	tokens = [ literal "Verify that Allocate car has completed " ]
{
}
creator.customaction qml_fab79999a8374f688824f19ced83d955
"Platform Transportation"
	interfaces = [ qml_53195b0799104c4fa334cb7ec577e763 ]
	shortform = "IA"
	direction = in
	tokens = [ literal "Start Platform Transportation" ]
{
}