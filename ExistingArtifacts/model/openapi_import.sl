com.conformiq.creator.structure.v15
creator.externalinterface
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
"http://petstore.swagger.io/api request"
	direction = in;
creator.externalinterface
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
"http://petstore.swagger.io/api response"
	direction = out;
creator.message
qml_openapi_import__GET_20__2f_pets_3f_tags_3d__7b_tags_7d__26_limit_3d__7b_limit_7d__20_Request__message
"GET /pets?tags={tags}&limit={limit} Request"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_3f_tags_3d__7b_tags_7d__26_limit_3d__7b_limit_7d__20_Request__primitivefield__tags
	"tags"
		type = String
		optional array;
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_3f_tags_3d__7b_tags_7d__26_limit_3d__7b_limit_7d__20_Request__primitivefield__limit
	"limit"
		type = number
		optional;
}
creator.message
qml_openapi_import__GET_20__2f_pets_20_Response_20__5b_200_5d___message
"GET /pets Response [200]"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_20_Response_20__5b_200_5d___primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__GET_20__2f_pets_20_Response_20__5b_200_5d___structuredfield__body
	"body"
		type = qml_openapi_import__Pet__sequencetype
		optional array;
}
creator.sequencetype qml_openapi_import__Pet__sequencetype "Pet"
{
	creator.primitivefield qml_openapi_import__Pet__primitivefield__name "name"
		type = String;
	creator.primitivefield qml_openapi_import__Pet__primitivefield__tag "tag"
		type = String
		optional;
	creator.primitivefield qml_openapi_import__Pet__primitivefield__id "id"
		type = number;
}
creator.message qml_openapi_import__GET_20__2f_pets_20_Response__message
"GET /pets Response"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_20_Response__primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__GET_20__2f_pets_20_Response__structuredfield__body "body"
		type = qml_openapi_import__Error__sequencetype;
}
creator.sequencetype qml_openapi_import__Error__sequencetype "Error"
{
	creator.primitivefield qml_openapi_import__Error__primitivefield__code "code"
		type = number;
	creator.primitivefield qml_openapi_import__Error__primitivefield__message
	"message"
		type = String;
}
creator.message qml_openapi_import__POST_20__2f_pets_20_Request__message
"POST /pets Request"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
]
{
	creator.structuredfield
	qml_openapi_import__POST_20__2f_pets_20_Request__structuredfield__pet "pet"
		type = qml_openapi_import__NewPet__sequencetype;
}
creator.sequencetype qml_openapi_import__NewPet__sequencetype "NewPet"
{
	creator.primitivefield qml_openapi_import__NewPet__primitivefield__name
	"name"
		type = String;
	creator.primitivefield qml_openapi_import__NewPet__primitivefield__tag "tag"
		type = String
		optional;
}
creator.message
qml_openapi_import__POST_20__2f_pets_20_Response_20__5b_200_5d___message
"POST /pets Response [200]"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__POST_20__2f_pets_20_Response_20__5b_200_5d___primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__POST_20__2f_pets_20_Response_20__5b_200_5d___structuredfield__body
	"body"
		type = qml_openapi_import__Pet__sequencetype;
}
creator.message qml_openapi_import__POST_20__2f_pets_20_Response__message
"POST /pets Response"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__POST_20__2f_pets_20_Response__primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__POST_20__2f_pets_20_Response__structuredfield__body
	"body"
		type = qml_openapi_import__Error__sequencetype;
}
creator.message
qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Request__message
"GET /pets/{id} Request"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Request__primitivefield__id
	"id"
		type = number;
}
creator.message
qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_200_5d___message
"GET /pets/{id} Response [200]"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_200_5d___primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_200_5d___structuredfield__body
	"body"
		type = qml_openapi_import__Pet__sequencetype;
}
creator.message
qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response__message
"GET /pets/{id} Response"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response__primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__GET_20__2f_pets_2f__7b_id_7d__20_Response__structuredfield__body
	"body"
		type = qml_openapi_import__Error__sequencetype;
}
creator.message
qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Request__message
"DELETE /pets/{id} Request"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_request__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Request__primitivefield__id
	"id"
		type = number;
}
creator.message
qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_204_5d___message
"DELETE /pets/{id} Response [204]"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response_20__5b_204_5d___primitivefield__status
	"status"
		type = number;
}
creator.message
qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response__message
"DELETE /pets/{id} Response"
	interfaces = [
qml_openapi_import__http_3a__2f__2f_petstore_2e_swagger_2e_io_2f_api_20_response__externalinterface
]
{
	creator.primitivefield
	qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response__primitivefield__status
	"status"
		type = number;
	creator.structuredfield
	qml_openapi_import__DELETE_20__2f_pets_2f__7b_id_7d__20_Response__structuredfield__body
	"body"
		type = qml_openapi_import__Error__sequencetype;
}