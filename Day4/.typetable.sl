com.conformiq.creator.structure.v15
creator.gui.screen qmlf34a7c960e5048819e856bf65d7717e7 "Screen widgets"
{
	creator.gui.button qml528fce99a8b4442f8d967727313e892a "Btn"
		status = dontcare;
	creator.gui.hyperlink qml7db3679777dd4804befb7c4cbfc6c6f8 "Hyperlink"
		status = dontcare;
	creator.gui.form qmlea15a03f5d59450ba900415b6d61261d "Form"
	{
		creator.gui.button qmlf505e86a3bca45d7a5a1b8d169b38cfc "Btn"
			status = dontcare;
		creator.gui.hyperlink qmlfd32189d9eef4b55a82083aaf95ad5d5 "Link"
			status = dontcare;
		creator.gui.labelwidget qmldb14be2c18ed4b1582a9709507d155cd "Label"
			status = dontcare;
		creator.gui.textbox qml4b21ac3915ea43a697865c30b75c5dd3 "TB"
			type = String
			status = dontcare;
		creator.gui.checkbox qml2f7cce8827034a99bc8adbd8333e0e3e "CB"
			status = dontcare
			checked = dontcare;
		creator.gui.dropdown qmlb087641b3ac24b5c999962ecbae88c49 "DD"
			type = qmlec47915944814a45898c7ea112fdafa4
			status = dontcare;
		creator.gui.radiobutton qml47cd5b26455245cb929e831868939945 "RB"
			annotations = [ "selenium:" = "";
		]
			type = qmlec47915944814a45898c7ea112fdafa4
			status = dontcare;
		creator.gui.calendar qmla9fca0cf0f51440cadfbae1800c3f6cf "Calendar"
			status = dontcare;
		creator.gui.listbox qml570e6dd2b6f2477082158c54ec1fd4e5 "LB"
			status = dontcare
			items = [ ];
	}
	creator.gui.labelwidget qmle1faa6788f1c4c8fb654eb2f1fed74e8 "Label"
		status = dontcare;
	creator.gui.menubar qml60dd0f69534049beab1cd005905c685d "MenuBar"
	{
		creator.gui.menu qml906cc078d9674d51a63c94724000ed8e "File"
		{
			creator.gui.menu qmlb8bba250bb154fbca7fc1edc105a670c "New"
			{
				creator.gui.clickchoice qmlf6a4118c250b42fd88712272d04f646b "Project";
				creator.gui.clickchoice qmld94505cd79054286936cfdf944cc8215 "Example";
			}
		}
	}
	creator.gui.tab qml5ad47ab402594bd89cbafa7877a6524b "Modelling"
		selected
	{
		creator.gui.group qmleeda17faa4a241709af79c75fd35ee6d "Explore view"
		{
			creator.gui.tab qml89e760593f2040b9894001a522d8fc85 "Project Explorer"
				selected
			{
				creator.gui.treenode qmldab476c4e89242d59858cc1f49aa2ee3 "Day1"
				{
					creator.gui.treenode qmlfa37044f1ac446bc87e07900642bf102 "DC"
					{
					}
				}
			}
		}
	}
	creator.gui.group qml3ce7d9507b5948dc99d5d93cb9781a02 "ATM Services"
	{
		creator.gui.hyperlink qml33fb0e630dd84e5fb2b0c37d6ece527c "Withdraw"
			status = dontcare;
	}
	creator.gui.treenode qmlc0eacac5bc5f46d1a49c543c8d946dde "TreeNode"
	{
		creator.gui.treenode qml7c8f3970a9034dafb88b86aa8f907043 "Day1"
		{
			creator.gui.treenode qml3b9cb911e61f454888fd23dd71c33860 "DC"
			{
			}
			creator.gui.treenode qml861dc9da2cb64243b1e182b410bc9c6f "Model"
			{
			}
		}
	}
}
creator.enum qmlec47915944814a45898c7ea112fdafa4 "Enum"
{
	creator.enumerationvalue qml6b24846afc994555892ea0cad1752d5a "V1";
}
creator.gui.screen qmlb27750dde7cd41848dcd3392c5aee6dc "Parabank Login"
{
	creator.gui.form qmlf8a77908353b41bab4f2e8ab9e94d784 "Customer Login"
	{
		creator.gui.textbox qmlb1cbdf14f8ff40b8ba95a115009737b2 "Username"
			annotations = [ "selenium:xpath" =
		"//*[@id=\"loginPanel\"]/form/div[1]/input";
		]
			type = String
			status = dontcare;
		creator.gui.textbox qml945124b492bb458d983bc5e9abda942b "Password"
			annotations = [ "selenium:xpath" =
		"//*[@id=\"loginPanel\"]/form/div[2]/input";
		]
			type = String
			status = dontcare;
	}
	creator.gui.button qmlda59caa5148047ea932dd4cb85d343c1 "Login"
		status = dontcare;
	creator.gui.hyperlink qml1dc8c5c3de3b45158ef89a50a6372e8a "Register"
		status = dontcare;
}
creator.gui.screen qmlbd862f146eaa41fcbc79045512641b73 "Home"
{
	creator.gui.hyperlink qmlf3c77c414aa74fdb9dc549ed29e84024 "Open New Account"
		status = dontcare;
	creator.gui.hyperlink qmlaba11f385d3b4964bd17c18b968f6894 "Transfer Funds"
		status = dontcare;
	creator.gui.hyperlink qml3f8db28bbaf141a0ba82381a50c0f0d2 "Logout"
		status = dontcare;
}
creator.gui.screen qml6ad7aac78781462d813e49e4d4d15d86 "Open New Account"
{
	creator.gui.form qml38b93052663f47b8ab329722077aaa00 "Open New Account"
	{
		creator.gui.dropdown qmlee3941f124ba439787ca48cc5c763472 "Type of Account"
			type = qml9f9d0cc73a494c4f92823222a8b960f9
			status = dontcare;
		creator.gui.dropdown qml4bb1db8267f9419c8a6ffdc90c6da39d "Existing Account"
			type = qml5e55d9a9cffa40668ded86debeef602b
			status = dontcare;
	}
	creator.gui.button qml054bd884d87947279ae595786760e99b "Open New Account"
		status = dontcare;
}
creator.enum qml9f9d0cc73a494c4f92823222a8b960f9 "Account Type"
{
	creator.enumerationvalue qml9c4bd3f77bdf4921ac7bef5258ba2dde "Checking";
	creator.enumerationvalue qmla8fd0561105240138be2f94c95b3e32f "Savings";
}
creator.enum qml5e55d9a9cffa40668ded86debeef602b "Exicsting Account"
{
	creator.enumerationvalue qmle98f76e0980e4224af507dc1e6c35fa1 "123654";
}
creator.gui.screen qmlaf96b14834194934bced315a8fa8ea3c "Transfer Funds"
{
	creator.gui.form qmld40308ebe6b34be9bbda8e928e0e6aec "Transfer Funds"
	{
		creator.gui.textbox qmlb55d64ddd6614a1ea54d4f9e75414c8c "Amount"
			type = String
			status = dontcare;
		creator.gui.dropdown qmldac9cff68ba245ca9630b52c08e0802f "From Account"
			type = qml5e55d9a9cffa40668ded86debeef602b
			status = dontcare;
		creator.gui.dropdown qml55216090146d455ea15368e4c00d9e9a "To Account"
			type = qml5e55d9a9cffa40668ded86debeef602b
			status = dontcare;
	}
	creator.gui.button qml71f9e14ece184254bc302614e04a89e4 "Transfer"
		status = dontcare;
}
creator.customaction qml0a126480f9544b1285cefabc5ec869d3 "Language"
	interfaces = [ qml77613dee9eb0420fb550d05e9f344dfd ]
	shortform = "LI"
	direction = in
	tokens = [ literal "Application is in " reference
qml4c60eae96db84d0f8a914d5c07f10c5e ]
{
	creator.primitivefield qml4c60eae96db84d0f8a914d5c07f10c5e "Language"
		type = String;
}
creator.externalinterface qml77613dee9eb0420fb550d05e9f344dfd "User"
	direction = in;
creator.customaction qmlc58531349e5a492cb661eb67f0348118 "Enter Url"
	interfaces = [ qml77613dee9eb0420fb550d05e9f344dfd ]
	shortform = "EU"
	direction = in
	tokens = [ reference qml0645e79ad0ca4864917467123b3915a0 ]
{
	creator.primitivefield qml0645e79ad0ca4864917467123b3915a0 "Url"
		type = String;
}