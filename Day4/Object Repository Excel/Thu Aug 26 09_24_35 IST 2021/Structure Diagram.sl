com.conformiq.creator.structure.v15
creator.gui.screen qmlc244275e5d6642268bc0a504df82918e "Test"
	deleted
{
	creator.gui.button qml4cd3e2d41ed94a35bb8ef0d655ecc8cd "Btn"
		status = dontcare
		deleted;
}
creator.gui.screen qml9f89dfc2981a447b9a1e793107507205 "unnamed"
	deleted
{
}
creator.gui.screen qmlb27750dde7cd41848dcd3392c5aee6dc "Parabank Login"
{
	creator.gui.form qmlf8a77908353b41bab4f2e8ab9e94d784 "Customer Login"
	{
		creator.gui.textbox qmlb1cbdf14f8ff40b8ba95a115009737b2 "Username"
			type = String
			status = dontcare;
		creator.gui.textbox qml945124b492bb458d983bc5e9abda942b "Password"
			type = String
			status = dontcare;
		creator.gui.button qml7ac2b063ae5a47f484bcc6d8e8c32ed6 "Login"
			status = dontcare
			deleted;
	}
	creator.gui.button qmlda59caa5148047ea932dd4cb85d343c1 "Login"
		status = dontcare;
	creator.gui.hyperlink qmla52a6924f78949cb98cdb49428c70c6e "ATM Services"
		status = dontcare
		deleted;
	creator.gui.hyperlink qml1dc8c5c3de3b45158ef89a50a6372e8a "Register"
		status = dontcare;
}
creator.gui.screen qml1e809d6c3f864f499051cc0dd3f9cf1a "ATM Services"
	deleted
{
}
creator.gui.screen qmlf7d1b0a0ed814cdb8d424666ceb7dcc1 "Login Page"
	deleted
{
}
creator.gui.screen qmlb9dcd1a7b6a84b5c9f9d0c47d74e1f49 "Login Screen"
	deleted
{
}
creator.gui.screen qmlbd862f146eaa41fcbc79045512641b73 "Home"
{
	creator.gui.hyperlink qmlf3c77c414aa74fdb9dc549ed29e84024 "Open New Account"
		status = dontcare;
	creator.gui.hyperlink qmlaba11f385d3b4964bd17c18b968f6894 "Transfer Funds"
		status = dontcare;
	creator.gui.hyperlink qml3f8db28bbaf141a0ba82381a50c0f0d2 "Logout"
		status = dontcare;
}
creator.gui.screen qml6ad7aac78781462d813e49e4d4d15d86 "Open New Account"
{
	creator.gui.form qml38b93052663f47b8ab329722077aaa00 "Open New Account"
	{
		creator.gui.dropdown qmlee3941f124ba439787ca48cc5c763472 "Type of Account"
			type = qml9f9d0cc73a494c4f92823222a8b960f9
			status = dontcare;
		creator.gui.dropdown qml4bb1db8267f9419c8a6ffdc90c6da39d "Existing Account"
			type = qml5e55d9a9cffa40668ded86debeef602b
			status = dontcare;
	}
	creator.gui.button qml054bd884d87947279ae595786760e99b "Open New Account"
		status = dontcare;
}
creator.enum qml9f9d0cc73a494c4f92823222a8b960f9 "Account Type"
{
	creator.enumerationvalue qml9c4bd3f77bdf4921ac7bef5258ba2dde "Checking";
	creator.enumerationvalue qmla8fd0561105240138be2f94c95b3e32f "Savings";
}
creator.enum qml5e55d9a9cffa40668ded86debeef602b "Exicsting Account"
{
	creator.enumerationvalue qmle98f76e0980e4224af507dc1e6c35fa1 "123654";
}
creator.gui.screen qmlaf96b14834194934bced315a8fa8ea3c "Transfer Funds"
{
	creator.gui.form qmld40308ebe6b34be9bbda8e928e0e6aec "Transfer Funds"
	{
		creator.gui.textbox qmlb55d64ddd6614a1ea54d4f9e75414c8c "Amount"
			type = String
			status = dontcare;
		creator.gui.dropdown qmldac9cff68ba245ca9630b52c08e0802f "From Account"
			type = qml5e55d9a9cffa40668ded86debeef602b
			status = dontcare;
		creator.gui.dropdown qml55216090146d455ea15368e4c00d9e9a "To Account"
			type = qml5e55d9a9cffa40668ded86debeef602b
			status = dontcare;
	}
	creator.gui.button qml71f9e14ece184254bc302614e04a89e4 "Transfer"
		status = dontcare;
}
creator.gui.screen qml8ad7a43a584f42faa7d3ab41dee03943 "Modelling"
	deleted
{
}
creator.customaction qml0a126480f9544b1285cefabc5ec869d3 "Language"
	interfaces = [ qml77613dee9eb0420fb550d05e9f344dfd ]
	shortform = "LI"
	direction = in
	tokens = [ literal "Application is in " reference
qml4c60eae96db84d0f8a914d5c07f10c5e ]
{
	creator.primitivefield qml4c60eae96db84d0f8a914d5c07f10c5e "Language"
		type = String;
}
creator.externalinterface qml77613dee9eb0420fb550d05e9f344dfd "User"
	direction = in;
creator.customaction qmlc58531349e5a492cb661eb67f0348118 "Enter Url"
	interfaces = [ qml77613dee9eb0420fb550d05e9f344dfd ]
	shortform = "EU"
	direction = in
	tokens = [ reference qml0645e79ad0ca4864917467123b3915a0 ]
{
	creator.primitivefield qml0645e79ad0ca4864917467123b3915a0 "Url"
		type = String;
}