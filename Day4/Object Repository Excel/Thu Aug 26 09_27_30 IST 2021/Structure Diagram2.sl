com.conformiq.creator.structure.v15
creator.gui.screen qmlf34a7c960e5048819e856bf65d7717e7 "Screen widgets"
{
	creator.gui.button qml528fce99a8b4442f8d967727313e892a "Btn"
		status = dontcare;
	creator.gui.hyperlink qml7db3679777dd4804befb7c4cbfc6c6f8 "Hyperlink"
		status = dontcare;
	creator.gui.form qmlea15a03f5d59450ba900415b6d61261d "Form"
	{
		creator.gui.button qmlf505e86a3bca45d7a5a1b8d169b38cfc "Btn"
			status = dontcare;
		creator.gui.hyperlink qmlfd32189d9eef4b55a82083aaf95ad5d5 "Link"
			status = dontcare;
		creator.gui.labelwidget qmldb14be2c18ed4b1582a9709507d155cd "Label"
			status = dontcare;
		creator.gui.textbox qml4b21ac3915ea43a697865c30b75c5dd3 "TB"
			type = String
			status = dontcare;
		creator.gui.checkbox qml2f7cce8827034a99bc8adbd8333e0e3e "CB"
			status = dontcare
			checked = dontcare;
		creator.gui.dropdown qmlb087641b3ac24b5c999962ecbae88c49 "DD"
			type = qmlec47915944814a45898c7ea112fdafa4
			status = dontcare;
		creator.gui.radiobutton qml47cd5b26455245cb929e831868939945 "RB"
	annotations = ["selenium:" = "";]
			type = qmlec47915944814a45898c7ea112fdafa4
			status = dontcare;
		creator.gui.calendar qmla9fca0cf0f51440cadfbae1800c3f6cf "Calendar"
			status = dontcare;
		creator.gui.listbox qml570e6dd2b6f2477082158c54ec1fd4e5 "LB"
			status = dontcare
			items = [ ];
	}
	creator.gui.labelwidget qmle1faa6788f1c4c8fb654eb2f1fed74e8 "Label"
		status = dontcare;
	creator.gui.menubar qml60dd0f69534049beab1cd005905c685d "MenuBar"
	{
		creator.gui.menu qml906cc078d9674d51a63c94724000ed8e "File"
		{
			creator.gui.menu qmlb8bba250bb154fbca7fc1edc105a670c "New"
			{
				creator.gui.clickchoice qmlf6a4118c250b42fd88712272d04f646b "Project";
				creator.gui.clickchoice qmld94505cd79054286936cfdf944cc8215 "Example";
			}
		}
	}
	creator.gui.tab qml5ad47ab402594bd89cbafa7877a6524b "Modelling"
		selected
	{
		creator.gui.group qmleeda17faa4a241709af79c75fd35ee6d "Explore view"
		{
			creator.gui.tab qml89e760593f2040b9894001a522d8fc85 "Project Explorer"
				selected
			{
				creator.gui.treenode qmldab476c4e89242d59858cc1f49aa2ee3 "Day1"
				{
					creator.gui.treenode qmlfa37044f1ac446bc87e07900642bf102 "DC"
					{
					}
				}
			}
		}
	}
	creator.gui.group qml3ce7d9507b5948dc99d5d93cb9781a02 "ATM Services"
	{
		creator.gui.hyperlink qml33fb0e630dd84e5fb2b0c37d6ece527c "Withdraw"
			status = dontcare;
	}
	creator.gui.treenode qmlc0eacac5bc5f46d1a49c543c8d946dde "TreeNode"
	{
		creator.gui.treenode qml7c8f3970a9034dafb88b86aa8f907043 "Day1"
		{
			creator.gui.treenode qml3b9cb911e61f454888fd23dd71c33860 "DC"
			{
			}
			creator.gui.treenode qml861dc9da2cb64243b1e182b410bc9c6f "Model"
			{
			}
		}
	}
}
creator.enum qmlec47915944814a45898c7ea112fdafa4 "Enum"
{
	creator.enumerationvalue qml6b24846afc994555892ea0cad1752d5a "V1";
}