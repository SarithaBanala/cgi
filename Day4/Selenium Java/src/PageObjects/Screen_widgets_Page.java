package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Screen_widgets_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Btn")
	public static WebElement Btn;

public void verify_Btn_Status(String data){
		//Verifies the Status of the Btn
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Btn.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Btn.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Btn.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Btn.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Btn(){
		Btn.click();
}

@FindBy(how= How.ID, using = "Hyperlink")
	public static WebElement Hyperlink;

public void verify_Hyperlink_Status(String data){
		//Verifies the Status of the Hyperlink
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Hyperlink.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Hyperlink.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Hyperlink.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Hyperlink.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Hyperlink(){
		Hyperlink.click();
}

@FindBy(how= How.ID, using = "Link")
	public static WebElement Link;

public void verify_Link_Status(String data){
		//Verifies the Status of the Link
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Link.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Link.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Link.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Link.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Link(){
		Link.click();
}

@FindBy(how= How.ID, using = "Label")
	public static WebElement Label;

public void verify_Label(String data){
		Assert.assertEquals(Label,Label);
}

public void verify_Label_Status(String data){
		//Verifies the Status of the Label
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Label.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Label.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Label.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Label.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "TB")
	public static WebElement TB;

public void verify_TB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(TB.getAttribute("value"),data);
	}

}

public void verify_TB_Status(String data){
		//Verifies the Status of the TB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(TB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(TB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!TB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!TB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_TB(String data){
		TB.clear();
		TB.sendKeys(data);
}

@FindBy(how= How.ID, using = "CB")
	public static WebElement CB;

public void verify_CB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(CB.getAttribute("value"),data);
	}

}

public void verify_CB_Status(String data){
		//Verifies the Status of the CB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(CB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(CB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!CB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!CB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_CB(String data){
		if(CB.isSelected());
			CB.click();
}

@FindBy(how= How.ID, using = "DD")
	public static WebElement DD;

public void verify_DD(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(DD.getAttribute("value"),data);
	}

}

public void verify_DD_Status(String data){
		//Verifies the Status of the DD
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(DD.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(DD.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!DD.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!DD.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_DD(String data){
		Select dropdown= new Select(DD);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "")
	public static WebElement RB;

public void verify_RB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(RB.getAttribute("name"),data);
	}

}

public void verify_RB_Status(String data){
		//Verifies the Status of the RB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(RB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(RB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!RB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!RB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
@FindBy(how= How.ID, using = "Calendar")
	public static WebElement Calendar;

public void verify_Calendar(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Calendar.getAttribute("value"),data);
	}

}

public void verify_Calendar_Status(String data){
		//Verifies the Status of the Calendar
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Calendar.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Calendar.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Calendar.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Calendar.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Calendar(String data){
		Calendar.click();
}

@FindBy(how= How.ID, using = "LB")
	public static WebElement LB;

public void verify_LB(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(LB.getAttribute("value"),data);
	}

}

public void verify_LB_Status(String data){
		//Verifies the Status of the LB
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(LB.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(LB.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!LB.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!LB.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_LB(String data){
		LB.click();
}

@FindBy(how= How.ID, using = "MenuBar")
	public static WebElement MenuBar;

public void click_MenuBar(){
		MenuBar.click();
}

@FindBy(how= How.ID, using = "File")
	public static WebElement File;

public void click_File(){
		File.click();
}

@FindBy(how= How.ID, using = "New")
	public static WebElement New;

public void click_New(){
		New.click();
}

@FindBy(how= How.ID, using = "Project")
	public static WebElement Project;

public void click_Project(){
		Project.click();
}

@FindBy(how= How.ID, using = "Example")
	public static WebElement Example;

public void click_Example(){
		Example.click();
}

@FindBy(how= How.ID, using = "Modelling")
	public static WebElement Modelling;

public void click_Modelling(){
		Modelling.click();
}

@FindBy(how= How.ID, using = "Explore_view")
	public static WebElement Explore_view;

public void click_Explore_view(){
		Explore_view.click();
}

@FindBy(how= How.ID, using = "Project_Explorer")
	public static WebElement Project_Explorer;

public void click_Project_Explorer(){
		Project_Explorer.click();
}

@FindBy(how= How.ID, using = "Day1")
	public static WebElement Day1;

public void click_Day1(){
		Day1.click();
}

@FindBy(how= How.ID, using = "DC")
	public static WebElement DC;

public void click_DC(){
		DC.click();
}

@FindBy(how= How.ID, using = "ATM_Services")
	public static WebElement ATM_Services;

public void click_ATM_Services(){
		ATM_Services.click();
}

@FindBy(how= How.ID, using = "Withdraw")
	public static WebElement Withdraw;

public void verify_Withdraw_Status(String data){
		//Verifies the Status of the Withdraw
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Withdraw.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Withdraw.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Withdraw.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Withdraw.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Withdraw(){
		Withdraw.click();
}

@FindBy(how= How.ID, using = "TreeNode")
	public static WebElement TreeNode;

public void click_TreeNode(){
		TreeNode.click();
}

@FindBy(how= How.ID, using = "Model")
	public static WebElement Model;

public void click_Model(){
		Model.click();
}

@FindBy(how= How.ID, using = "selenium:"_=_"_V1")
	public WebElement selenium_______V1;

public void verify_selenium_______V1_Status(String data){
		//Verifies the Status of the selenium_______V1
		Assert.assertEquals(selenium_______V1,selenium_______V1);
}

public void select_selenium_______V1(){
		selenium_______V1.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}