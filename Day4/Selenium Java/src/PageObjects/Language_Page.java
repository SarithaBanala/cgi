package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Language_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Language")
	public static WebElement Language;

public void verify_Language(String data){
		Assert.assertEquals(Language,Language);
}

public void enter_Language(String data){
		Language.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}