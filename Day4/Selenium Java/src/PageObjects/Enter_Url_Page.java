package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Enter_Url_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Url")
	public static WebElement Url;

public void verify_Url(String data){
		Assert.assertEquals(Url,Url);
}

public void enter_Url(String data){
		driver.get(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}