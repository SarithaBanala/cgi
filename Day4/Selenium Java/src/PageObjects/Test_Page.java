package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Test_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Btn")
	public static WebElement Btn;

public void verify_Btn_Status(String data){
		//Verifies the Status of the Btn
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Btn.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Btn.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Btn.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Btn.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Btn(){
		Btn.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}