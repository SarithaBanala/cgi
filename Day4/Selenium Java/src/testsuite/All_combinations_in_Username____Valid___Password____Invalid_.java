package testsuite;
import org.testng.annotations.Test;
import PageObjects.*;
import utilities.PageObjectBase;
import org.openqa.selenium.support.PageFactory;
import utilities.Configurations;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import utilities.TestReport;
import java.io.IOException;
import org.testng.Reporter;
import utilities.DataUtil;


/** Conformiq generated test case
	All_combinations_in_Username____Valid___Password____Invalid_
*/
public class All_combinations_in_Username____Valid___Password____Invalid_ extends PageObjectBase
{

	public All_combinations_in_Username____Valid___Password____Invalid_()
	{
	}

	private TestReport testReport= new TestReport();


	private StringBuilder overallTestData= new StringBuilder();


	@Test(dataProvider="TestData")
	public void test(final String Step_1_Username_TEXTBOX_Status,final String Step_1_Username_TEXTBOX_Verification,final String Step_1_Password_TEXTBOX_Status,final String Step_1_Password_TEXTBOX_Verification,final String Step_1_Login_BUTTON_Status,final String Step_1_Register_HYPERLINK_Status,final String Step_2_Username_TEXTBOX,final String Step_2_Password_TEXTBOX,final String Step_4_Username_TEXTBOX_Status,final String Step_4_Username_TEXTBOX_Verification,final String Step_4_Password_TEXTBOX_Status,final String Step_4_Password_TEXTBOX_Verification,final String Step_4_Login_BUTTON_Status,final String Step_4_Register_HYPERLINK_Status) throws Exception

	{

	Parabank_Login_Page parabank_login_page_init=PageFactory.initElements(driver, Parabank_Login_Page.class);

	Home_Page home_page_init=PageFactory.initElements(driver, Home_Page.class);

	Open_New_Account_Page open_new_account_page_init=PageFactory.initElements(driver, Open_New_Account_Page.class);

	Transfer_Funds_Page transfer_funds_page_init=PageFactory.initElements(driver, Transfer_Funds_Page.class);

	Language_Page language_page_init=PageFactory.initElements(driver, Language_Page.class);

	Enter_Url_Page enter_url_page_init=PageFactory.initElements(driver, Enter_Url_Page.class);

	Screen_widgets_Page screen_widgets_page_init=PageFactory.initElements(driver, Screen_widgets_Page.class);

	global_Circumstance_Page global_circumstance_page_init= PageFactory.initElements(driver, global_Circumstance_Page.class);

	testReport.createTesthtmlHeader(overallTestData);

	testReport.createHead(overallTestData);

	testReport.putLogo(overallTestData);

	float ne = (float) 0.0;

	testReport.generateGeneralInfo(overallTestData, "All_combinations_in_Username____Valid___Password____Invalid_", "TC_All_combinations_in_Username____Valid___Password____Invalid_", "",ne);

	testReport.createStepHeader();

	//External Circumstances

	global_circumstance_page_init.global_circumstance("French");


	Reporter.log("Step - 1- verify Parabank Login screen");

	testReport.fillTableStep("Step 1", "verify Parabank Login screen");

	parabank_login_page_init.verify_Username_Status(Step_1_Username_TEXTBOX_Status);

	parabank_login_page_init.verify_Username(Step_1_Username_TEXTBOX_Verification);
	parabank_login_page_init.verify_Password_Status(Step_1_Password_TEXTBOX_Status);

	parabank_login_page_init.verify_Password(Step_1_Password_TEXTBOX_Verification);
	parabank_login_page_init.verify_Login_Status(Step_1_Login_BUTTON_Status);

	parabank_login_page_init.verify_Register_Status(Step_1_Register_HYPERLINK_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "All_combinations_in_Username____Valid___Password____Invalid_","Step_1");

	Reporter.log("Step - 2- Fill Customer Login form Parabank Login screen");

	testReport.fillTableStep("Step 2", "Fill Customer Login form Parabank Login screen");

	parabank_login_page_init.set_Username(Step_2_Username_TEXTBOX);
	parabank_login_page_init.set_Password(Step_2_Password_TEXTBOX);
	getScreenshot(driver,Configurations.screenshotLocation , "All_combinations_in_Username____Valid___Password____Invalid_","Step_2");

	Reporter.log("Step - 3- click Login button Parabank Login screen");

	testReport.fillTableStep("Step 3", "click Login button Parabank Login screen");

	parabank_login_page_init.click_Login();
	getScreenshot(driver,Configurations.screenshotLocation , "All_combinations_in_Username____Valid___Password____Invalid_","Step_3");

	Reporter.log("Step - 4- verify Parabank Login screen");

	testReport.fillTableStep("Step 4", "verify Parabank Login screen");

	parabank_login_page_init.verify_Username_Status(Step_4_Username_TEXTBOX_Status);

	parabank_login_page_init.verify_Username(Step_4_Username_TEXTBOX_Verification);
	parabank_login_page_init.verify_Password_Status(Step_4_Password_TEXTBOX_Status);

	parabank_login_page_init.verify_Password(Step_4_Password_TEXTBOX_Verification);
	parabank_login_page_init.verify_Login_Status(Step_4_Login_BUTTON_Status);

	parabank_login_page_init.verify_Register_Status(Step_4_Register_HYPERLINK_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "All_combinations_in_Username____Valid___Password____Invalid_","Step_4");
	}
	@DataProvider(name = "TestData")
	public Object[][] getData() {
	return DataUtil.getDataFromSpreadSheet("TestData.xlsx", "TCID_2");
}
	@AfterTest
	public void export(){
		testReport.appendtestData(overallTestData);
		testReport.closeStepTable();
		testReport.closeTestHTML(overallTestData);
		driver.close();
		try {
			testReport.writeTestReporthtml(overallTestData, "All_combinations_in_Username____Valid___Password____Invalid_");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
