package testsuite;
import org.testng.annotations.Test;
import PageObjects.*;
import utilities.PageObjectBase;
import org.openqa.selenium.support.PageFactory;
import utilities.Configurations;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import utilities.TestReport;
import java.io.IOException;
import org.testng.Reporter;
import utilities.DataUtil;


/** Conformiq generated test case
	Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_
*/
public class Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_ extends PageObjectBase
{

	public Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_()
	{
	}

	private TestReport testReport= new TestReport();


	private StringBuilder overallTestData= new StringBuilder();


	@Test(dataProvider="TestData")
	public void test(final String Step_1_Username_TEXTBOX_Status,final String Step_1_Username_TEXTBOX_Verification,final String Step_1_Password_TEXTBOX_Status,final String Step_1_Password_TEXTBOX_Verification,final String Step_1_Login_BUTTON_Status,final String Step_1_Register_HYPERLINK_Status,final String Step_2_Username_TEXTBOX,final String Step_2_Password_TEXTBOX,final String Step_4_Open_New_Account_HYPERLINK_Status,final String Step_4_Transfer_Funds_HYPERLINK_Status,final String Step_4_Logout_HYPERLINK_Status,final String Step_5_Language_FIELD,final String Step_6_Amount_TEXTBOX_Status,final String Step_6_Amount_TEXTBOX_Verification,final String Step_6_From_Account_DROPDOWN_Status,final String Step_6_From_Account_DROPDOWN_Verification,final String Step_6_To_Account_DROPDOWN_Status,final String Step_6_To_Account_DROPDOWN_Verification,final String Step_6_Transfer_BUTTON_Status) throws Exception

	{

	Parabank_Login_Page parabank_login_page_init=PageFactory.initElements(driver, Parabank_Login_Page.class);

	Home_Page home_page_init=PageFactory.initElements(driver, Home_Page.class);

	Open_New_Account_Page open_new_account_page_init=PageFactory.initElements(driver, Open_New_Account_Page.class);

	Transfer_Funds_Page transfer_funds_page_init=PageFactory.initElements(driver, Transfer_Funds_Page.class);

	Language_Page language_page_init=PageFactory.initElements(driver, Language_Page.class);

	Enter_Url_Page enter_url_page_init=PageFactory.initElements(driver, Enter_Url_Page.class);

	Screen_widgets_Page screen_widgets_page_init=PageFactory.initElements(driver, Screen_widgets_Page.class);

	global_Circumstance_Page global_circumstance_page_init= PageFactory.initElements(driver, global_Circumstance_Page.class);

	testReport.createTesthtmlHeader(overallTestData);

	testReport.createHead(overallTestData);

	testReport.putLogo(overallTestData);

	float ne = (float) 0.0;

	testReport.generateGeneralInfo(overallTestData, "Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_", "TC_Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_", "",ne);

	testReport.createStepHeader();

	//External Circumstances

	global_circumstance_page_init.global_circumstance("in English");


	Reporter.log("Step - 1- verify Parabank Login screen");

	testReport.fillTableStep("Step 1", "verify Parabank Login screen");

	parabank_login_page_init.verify_Username_Status(Step_1_Username_TEXTBOX_Status);

	parabank_login_page_init.verify_Username(Step_1_Username_TEXTBOX_Verification);
	parabank_login_page_init.verify_Password_Status(Step_1_Password_TEXTBOX_Status);

	parabank_login_page_init.verify_Password(Step_1_Password_TEXTBOX_Verification);
	parabank_login_page_init.verify_Login_Status(Step_1_Login_BUTTON_Status);

	parabank_login_page_init.verify_Register_Status(Step_1_Register_HYPERLINK_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_","Step_1");

	Reporter.log("Step - 2- Fill Customer Login form Parabank Login screen");

	testReport.fillTableStep("Step 2", "Fill Customer Login form Parabank Login screen");

	parabank_login_page_init.set_Username(Step_2_Username_TEXTBOX);
	parabank_login_page_init.set_Password(Step_2_Password_TEXTBOX);
	getScreenshot(driver,Configurations.screenshotLocation , "Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_","Step_2");

	Reporter.log("Step - 3- click Login button Parabank Login screen");

	testReport.fillTableStep("Step 3", "click Login button Parabank Login screen");

	parabank_login_page_init.click_Login();
	getScreenshot(driver,Configurations.screenshotLocation , "Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_","Step_3");

	Reporter.log("Step - 4- verify Home screen");

	testReport.fillTableStep("Step 4", "verify Home screen");

	home_page_init.verify_Open_New_Account_Status(Step_4_Open_New_Account_HYPERLINK_Status);

	home_page_init.verify_Transfer_Funds_Status(Step_4_Transfer_Funds_HYPERLINK_Status);

	home_page_init.verify_Logout_Status(Step_4_Logout_HYPERLINK_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_","Step_4");

	Reporter.log("Step - 5- Perform Language Action");

	testReport.fillTableStep("Step 5", "Perform Language Action");

	language_page_init.enter_Language(Step_5_Language_FIELD);


	Reporter.log("Step - 6- verify Transfer Funds screen");

	testReport.fillTableStep("Step 6", "verify Transfer Funds screen");

	transfer_funds_page_init.verify_Amount_Status(Step_6_Amount_TEXTBOX_Status);

	transfer_funds_page_init.verify_Amount(Step_6_Amount_TEXTBOX_Verification);
	transfer_funds_page_init.verify_From_Account_Status(Step_6_From_Account_DROPDOWN_Status);

	transfer_funds_page_init.verify_From_Account(Step_6_From_Account_DROPDOWN_Verification);
	transfer_funds_page_init.verify_To_Account_Status(Step_6_To_Account_DROPDOWN_Status);

	transfer_funds_page_init.verify_To_Account(Step_6_To_Account_DROPDOWN_Verification);
	transfer_funds_page_init.verify_Transfer_Status(Step_6_Transfer_BUTTON_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_","Step_6");
	}
	@DataProvider(name = "TestData")
	public Object[][] getData() {
	return DataUtil.getDataFromSpreadSheet("TestData.xlsx", "TCID_4");
}
	@AfterTest
	public void export(){
		testReport.appendtestData(overallTestData);
		testReport.closeStepTable();
		testReport.closeTestHTML(overallTestData);
		driver.close();
		try {
			testReport.writeTestReporthtml(overallTestData, "Decision__Decision_Node_2__via__else__to_Activity__Activity_Node_2_");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
